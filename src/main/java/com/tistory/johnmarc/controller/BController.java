package com.tistory.johnmarc.controller;

import com.tistory.johnmarc.command.*;
import com.tistory.johnmarc.utill.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 2016-09-06.
 */

@Controller
@RequestMapping("/johnmarc")
public class BController {

    BCommand command = null;
    public JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        Constant.jdbcTemplate = this.jdbcTemplate;
    }

    @RequestMapping("/list")
    public String list(Model model){
        System.out.println("list() execute!");
        command = new BListCommand();
        command.execute(model);
        return "list";
    }

    @RequestMapping("/write_view")
    public String write_view(Model model){
        System.out.println("write_view() execute!");
        return "write_view";
    }

    @RequestMapping("/write")
    public String write(HttpServletRequest request, Model model){
        System.out.println("write() execute!");
        model.addAttribute("request",request);
        command = new BWriteCommand();
        command.execute(model);
        return "redirect:list";
    }

    @RequestMapping("/content_view")
    public String content_view(HttpServletRequest request, Model model){
        System.out.println("content_view() execute!");
        model.addAttribute("request",request);
        command = new BContentCommand();
        command.execute(model);
        return "content_view";
    }

    @RequestMapping(value="/modify", method = RequestMethod.POST)
    public String modify(HttpServletRequest request, Model model){
        System.out.println("modify() execute!");
        model.addAttribute("request",request);
        command = new BModifyCommand();
        command.execute(model);
        return "redirect:list";
    }
}
