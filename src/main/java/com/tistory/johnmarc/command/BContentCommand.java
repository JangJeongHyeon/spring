package com.tistory.johnmarc.command;

import com.tistory.johnmarc.dao.BDao;
import com.tistory.johnmarc.dto.BDto;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by User on 2016-09-12.
 */
public class BContentCommand implements BCommand {
    public void execute(Model model) {
        Map<String, Object> map = model.asMap();
        HttpServletRequest request = (HttpServletRequest) map.get("request");
        String bId = request.getParameter("bId");

        BDao dao = new BDao();
        BDto dto = dao.contentView(bId);

        model.addAttribute("content_view", dto);
    }
}
