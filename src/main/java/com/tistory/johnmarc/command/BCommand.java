package com.tistory.johnmarc.command;

import org.springframework.ui.Model;

/**
 * Created by User on 2016-09-06.
 */
public interface BCommand {

    void execute(Model model);
}
