package com.tistory.johnmarc.command;

import com.tistory.johnmarc.dao.BDao;
import com.tistory.johnmarc.dto.BDto;
import org.springframework.ui.Model;

import java.util.ArrayList;

/**
 * Created by User on 2016-09-12.
 */
public class BListCommand implements BCommand{
    public void execute(Model model) {
        BDao bDao = new BDao();
        ArrayList<BDto> dtos = bDao.list();
        model.addAttribute("list",dtos);
    }
}
