package com.tistory.johnmarc.command;

import com.tistory.johnmarc.dao.BDao;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by User on 2016-09-12.
 */
public class BModifyCommand implements BCommand {
    public void execute(Model model) {
        Map<String, Object> map  = model.asMap();
        HttpServletRequest request = (HttpServletRequest)map.get("request");

        String bId = request.getParameter("bId");
        String bName = request.getParameter("bName");
        String bTitle = request.getParameter("bTitle");
        String bContent = request.getParameter("bContent");

        BDao bDao = new BDao();
        bDao.modify(bId,bName,bTitle,bContent);
    }
}
