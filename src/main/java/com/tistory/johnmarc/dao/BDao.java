package com.tistory.johnmarc.dao;

import com.tistory.johnmarc.dto.BDto;
import com.tistory.johnmarc.utill.Constant;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by User on 2016-09-06.
 */
public class BDao {
    JdbcTemplate template = null;

    public BDao() {

//      Initialize of the JDBC Template
        template = Constant.jdbcTemplate;
    }

//   게시물 목록보기
    public ArrayList<BDto> list() {
        String query = "select bId, bName, bTitle, bContent, bDate, bHit, bGroup, bStep, bIndent from mvc_board order by bGroup desc, bStep asc";
        return (ArrayList<BDto>) template.query(query, new BeanPropertyRowMapper<BDto>(BDto.class));
    }

//    게시물 작성
    public void write(final String bName, final String bTitle, final String bContent) {
            template.update(new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    String query = "insert into mvc_board (bName, bTitle, bContent, bHit, bGroup, bStep, bIndent) values (?,?,?,0,(select max(bId)+1 from mvc_board),0,0)";
                    PreparedStatement preparedStatement = connection.prepareStatement(query);
                    preparedStatement.setString(1,bName);
                    preparedStatement.setString(2,bTitle);
                    preparedStatement.setString(3,bContent);
                    return preparedStatement;
                }
            });
    }


//    게시물 보기
    public BDto contentView(String strID) {
        upHit(strID);
        String query = "select * from mvc_board where bId ="+strID;
        return template.queryForObject(query, new BeanPropertyRowMapper<BDto>(BDto.class));
    }

//    수정
    public void modify(final String bId, final String bName, final String bTitle, final String bContent) {
            String query = "update mvc_board set bName = ?, bTitle = ?, bContent = ? where bId = ?";
            template.update(query, new PreparedStatementSetter() {
                public void setValues(PreparedStatement preparedStatement) throws SQLException {
                    preparedStatement.setString(1,bName);
                    preparedStatement.setString(2,bTitle);
                    preparedStatement.setString(3,bContent);
                    preparedStatement.setInt(4,Integer.parseInt(bId));
                }
            });

    }

//    게시글 삭제
    public void delete(final String bId) {
        String query = "delete from mvc_board where bId = ?";
         template.update(query, new PreparedStatementSetter() {
             public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setInt(1,Integer.parseInt(bId));
             }
         });

    }

//    답변글 보기
    public BDto reply_view(String str) {

        String query = "select * from mvc_board where bId ="+str;
        return template.queryForObject(query, new BeanPropertyRowMapper<BDto>(BDto.class));
    }

//    답변달기
    public void reply(String bId, final String bName, final String bTitle, final String bContent, final String bGroup, final String bStep, final String bIndent) {
        String query = "insert into mvc_board (bName, bTitle, bContent, bGroup, bStep, bIndent) values (?, ?, ?, ?, ?, ?)";
        template.update(query, new PreparedStatementSetter() {
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1,bName);
                preparedStatement.setString(2,bTitle);
                preparedStatement.setString(3,bContent);
                preparedStatement.setInt(4,Integer.parseInt(bGroup));
                preparedStatement.setInt(5,Integer.parseInt(bStep));
                preparedStatement.setInt(6,Integer.parseInt(bIndent));
            }
        });
    }

//    답변 들여쓰기
    private void replyShape(final String strGroup, final String strStep) {
            String query = "update mvc_board set bStep = bStep + 1 where bGroup =  ? and bStep > ?";
            template.update(query, new PreparedStatementSetter() {
                public void setValues(PreparedStatement preparedStatement) throws SQLException {
                    preparedStatement.setInt(1,Integer.parseInt(strGroup));
                    preparedStatement.setInt(2,Integer.parseInt(strStep));
                }
            });
    }

//    조회수 증가하기
    private void upHit(final String bIdD) {

        String query = "update mvc_board set bHit = bHit +1 where bId = ";

        template.update(query, new PreparedStatementSetter() {
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
              preparedStatement.setInt(1,Integer.parseInt(bIdD));
            }
        });
    }
}
